package com.py.apificar.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.py.apificar.entity.Config;

public interface ConfigRepository extends CrudRepository<Config, Long> {

	/**
	 * Busca key por estado
	 */
	Optional<Config> findByFirmaAndEstado(String key, String estado);

}
