package com.py.apificar.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.py.apificar.error.AseguradoException;
import com.py.apificar.model.Asegurado;

public class HtmlConverter {
	
	public static Document generateHtml(Asegurado asegurado) {
		Document doc = Jsoup.parse("<html></html>");  
		doc.body().appendElement("div").appendElement("h1").text("Documento : " + asegurado.getPersona().getDocumento());
		doc.body().appendElement("div").appendElement("h1").text("Nombres : " + asegurado.getPersona().getNombres());
		doc.body().appendElement("div").appendElement("h1").text("Apellidos : " + asegurado.getPersona().getApellidos());
		doc.body().appendElement("div").appendElement("h1").text("Fecha Nacimiento : " + asegurado.getPersona().getFechaNacimiento());
		doc.body().appendElement("div").appendElement("h1").text("Sexo : " + asegurado.getPersona().getSexo());
		doc.body().appendElement("div").appendElement("h1").text("Tipo Asegurado : " + asegurado.getPersona().getTipoAsegurado());
		doc.body().appendElement("div").appendElement("h1").text("Beneficiarios Activos : " + asegurado.getPersona().getBeneficiariosActivos());
		doc.body().appendElement("div").appendElement("h1").text("Enrolado" + asegurado.getPersona().getEnrolado());
		doc.body().appendElement("div").appendElement("h1").text("Vencimiento Fe de Vida : " + asegurado.getPersona().getVencimientoFeVida());
		doc.body().appendElement("div").appendElement("h1").text("Nro Patronal : " + asegurado.getEmpleador().getNroPatronal());
		doc.body().appendElement("div").appendElement("h1").text("Empleador : " + asegurado.getEmpleador().getEmpleador());
		doc.body().appendElement("div").appendElement("h1").text("Estado : " + asegurado.getEmpleador().getEstado());
		doc.body().appendElement("div").appendElement("h1").text("Meses Aporte : " + asegurado.getEmpleador().getMesesAporte());
		doc.body().appendElement("div").appendElement("h1").text("Vencimiento : " + asegurado.getEmpleador().getVencimiento());
		doc.body().appendElement("div").appendElement("h1").text("Ultimo Periodo Abonado : " + asegurado.getEmpleador().getUltimoPeriodoAbonado());
		return doc;
	}
	
	
	public static Document generateErrorHtml(AseguradoException aseguradoException){
		Document doc = Jsoup.parse("<html></html>");
		doc.body().appendElement("div").appendElement("h1").appendText("Codigo : " + aseguradoException.getError().getCodigo() );
		doc.body().appendElement("div").appendElement("h1").appendText("Error : " + aseguradoException.getError().getError() );
		return doc;
	}

}
