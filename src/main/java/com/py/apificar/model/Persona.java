package com.py.apificar.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@XmlRootElement(name = "persona")
public class Persona implements Serializable{

	private static final long serialVersionUID = 1L;

	@JsonProperty("Documento")
	private String documento;
	
	@JsonProperty("Nombres")
	private String nombres;
	
	@JsonProperty("Apellidos")
	private String apellidos;
	
	@JsonProperty("Fecha Nacim")
	@JacksonXmlProperty(localName = "Fecha_Nacim")	
	private String fechaNacimiento;
	
	@JsonProperty("Sexo")
	private String sexo;
	
	@JsonProperty("Tipo Asegurado")
	@JacksonXmlProperty(localName = "Tipo_Asegurado")	
	private String tipoAsegurado;
	
	@JsonProperty("Beneficiarios Activos")
	@JacksonXmlProperty(localName = "Beneficiarios_Activos")	
	private String beneficiariosActivos;
	
	@JsonProperty("Enrolado")
	private String enrolado;
	
	@JsonProperty("Vencimiento de fe de vida")
	@JacksonXmlProperty(localName = "Vencimiento_de_fe_de_vida")
	private String vencimientoFeVida;

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTipoAsegurado() {
		return tipoAsegurado;
	}

	public void setTipoAsegurado(String tipoAsegurado) {
		this.tipoAsegurado = tipoAsegurado;
	}

	public String getBeneficiariosActivos() {
		return beneficiariosActivos;
	}

	public void setBeneficiariosActivos(String beneficiariosActivos) {
		this.beneficiariosActivos = beneficiariosActivos;
	}

	public String getEnrolado() {
		return enrolado;
	}

	public void setEnrolado(String enrolado) {
		this.enrolado = enrolado;
	}

	public String getVencimientoFeVida() {
		return vencimientoFeVida;
	}

	public void setVencimientoFeVida(String vencimientoFeVida) {
		this.vencimientoFeVida = vencimientoFeVida;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Persona [Documento=");
		builder.append(documento);
		builder.append(", Nombres=");
		builder.append(nombres);
		builder.append(", Apellidos=");
		builder.append(apellidos);
		builder.append(", fecha Nacimiento=");
		builder.append(fechaNacimiento);
		builder.append(", sexo=");
		builder.append(sexo);
		builder.append(", tipo Asegurado=");
		builder.append(tipoAsegurado);
		builder.append(", Beneficiarios Activos=");
		builder.append(beneficiariosActivos);
		builder.append(", Enrolado=");
		builder.append(enrolado);
		builder.append(", Vencimiento Fe de Vida=");
		builder.append(vencimientoFeVida);
		builder.append("]");
		return builder.toString();
	}


	

}
