package com.py.apificar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApificarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApificarApplication.class, args);
	}

}
