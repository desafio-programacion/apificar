package com.py.apificar.error;

import javax.servlet.http.HttpServletRequest;

import org.jsoup.nodes.Document;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.py.apificar.utils.HtmlConverter;

@ControllerAdvice
public class AseguradoExceptionHandler {

	@ExceptionHandler(AseguradoException.class)
	public ResponseEntity<?> handleContentNotAllowedException(AseguradoException aseguradoException,
			final HttpServletRequest req) {

		String header = req.getHeader("Accept");
		switch (header) {
			case MediaType.TEXT_PLAIN_VALUE:
				return new ResponseEntity<String>(aseguradoException.getError().toString(), aseguradoException.getStatus());
			case MediaType.TEXT_HTML_VALUE:
				Document doc = HtmlConverter.generateErrorHtml(aseguradoException);
				return new ResponseEntity<String>(doc.toString(), aseguradoException.getStatus());
			default:
				return new ResponseEntity<Error>(aseguradoException.getError(), aseguradoException.getStatus());
		}

	}

}
