package com.py.apificar.model;

import java.io.Serializable;

public class Asegurado implements Serializable {

	private static final long serialVersionUID = 1L;

	private Persona persona;
	private Empleador empleador;

	public Asegurado() {
	}

	public Asegurado(Persona persona, Empleador empleador) {
		super();
		this.persona = persona;
		this.empleador = empleador;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Empleador getEmpleador() {
		return empleador;
	}

	public void setEmpleador(Empleador empleador) {
		this.empleador = empleador;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Asegurado [persona=");
		builder.append(persona.toString());
		builder.append(", empleador=");
		builder.append(empleador.toString());
		builder.append("]");
		return builder.toString();
	}
	
	

}
