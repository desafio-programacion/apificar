package com.py.apificar.error;

import java.io.Serializable;

public class Error implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String error;

	public Error() {
	}

	public Error(String codigo, String error) {
		super();
		this.codigo = codigo;
		this.error = error;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Error [codigo=");
		builder.append(codigo);
		builder.append(", error=");
		builder.append(error);
		builder.append("]");
		return builder.toString();
	}

}
