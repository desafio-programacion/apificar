package com.py.apificar.service;

import com.py.apificar.error.AseguradoException;
import com.py.apificar.model.Asegurado;

public interface AseguradoService {

	/**
	 * Obtener datos de asegurado
	 * 
	 * @param cedula
	 * @return 
	 */
	public Asegurado getAsegurado(String cedula) throws AseguradoException  ;

	/**
	 * Verificar que exista usuario para cédula
	 * 
	 * @param cedula
	 * @return
	 * @throws AseguradoException 
	 */
	public boolean verificarCedula(String cedula) throws AseguradoException;

}
