package com.py.apificar.service.impl;

import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.py.apificar.enums.Errors;
import com.py.apificar.error.AseguradoException;
import com.py.apificar.error.Error;
import com.py.apificar.model.Asegurado;
import com.py.apificar.model.Empleador;
import com.py.apificar.model.Persona;
import com.py.apificar.service.AseguradoService;

@Service
public class AseguradoServiceImpl implements AseguradoService {

	private static final Logger logger = LoggerFactory.getLogger(AseguradoServiceImpl.class);

	@Value("${url}")
	private String url;

	@Override
	public Asegurado getAsegurado(String cedula) throws AseguradoException {
		try {
			Response response = null;
			try {
				response = obtenerDatos();
			} catch (Exception e) {
				throw new AseguradoException(
						new Error(Errors.INTERNAL_ERROR.getCode(), Errors.INTERNAL_ERROR.getDescription()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}

			if (response != null && response.statusCode() == 200) {

				Error error = validarCedula(cedula);
				if (error == null) {
					try {
						Elements datosAsegurado = consultaAsegurado(cedula);
						if (datosAsegurado != null) {
							return getDatosAsegurados(datosAsegurado, cedula);
						} else {
							throw new AseguradoException(
									new Error(Errors.NOT_EXIST.getCode(),
											Errors.USER.getDescription() + cedula + Errors.NOT_EXIST.getDescription()),
									HttpStatus.NOT_FOUND);
						}
					} catch (Exception e) {
						throw new AseguradoException(
								new Error(Errors.INTERNAL_ERROR.getCode(), Errors.INTERNAL_ERROR.getDescription()),
								HttpStatus.INTERNAL_SERVER_ERROR);
					}
				} else {
					logger.info("Parametros Inválidos");
					throw new AseguradoException(error, HttpStatus.BAD_REQUEST);
				}

			}
		} catch (AseguradoException e) {
			logger.error("Error : " + e.getError().getError());
			throw e;
		}
		return null;
	}

	private Asegurado getDatosAsegurados(Elements datosAsegurado, String cedula) throws AseguradoException {

		Persona persona = new Persona();
		if (datosAsegurado.get(7).text() == null || datosAsegurado.get(7).text().isEmpty()) {
			logger.info("Usuario con CI no existe ");
			throw new AseguradoException(
					new Error(Errors.NOT_EXIST.getCode(),
							Errors.USER.getDescription() + cedula + Errors.NOT_EXIST.getDescription()),
					HttpStatus.NOT_FOUND);
		}

		persona.setDocumento(datosAsegurado.get(7).text());
		persona.setNombres(datosAsegurado.get(8).text());
		persona.setApellidos(datosAsegurado.get(9).text());
		persona.setFechaNacimiento(datosAsegurado.get(10).text());
		persona.setSexo(datosAsegurado.get(11).text());
		persona.setTipoAsegurado(datosAsegurado.get(12).text());
		persona.setBeneficiariosActivos(datosAsegurado.get(13).text());
		persona.setEnrolado(datosAsegurado.get(14).text());
		persona.setVencimientoFeVida(datosAsegurado.get(15).text());

		Empleador empleador = new Empleador();
		empleador.setNroPatronal(datosAsegurado.get(16).text());
		empleador.setEmpleador(datosAsegurado.get(17).text());
		empleador.setEstado(datosAsegurado.get(18).text());
		empleador.setMesesAporte(datosAsegurado.get(19).text());
		empleador.setVencimiento(datosAsegurado.get(20).text());
		empleador.setUltimoPeriodoAbonado(datosAsegurado.get(21).text());

		Asegurado asegurado = new Asegurado();
		asegurado.setEmpleador(empleador);
		asegurado.setPersona(persona);

		logger.info("Se encontraron datos de asegurado");
		return asegurado;
	}

	private Elements consultaAsegurado(String cedula) throws Exception {
		try {
			Document doc = Jsoup.connect(url).data("nro_cic", cedula).data("recuperar", "Recuperar").data("envio", "ok")
					.userAgent("Mozilla/5.0").timeout(1000000).post();

			Elements datosAsegurado = doc.select("td");
			return datosAsegurado;

		} catch (Exception e) {
			throw e;
		}
	}

	private Error validarCedula(String cedula) {
		Error error = null;
		try {
			Integer.parseInt(cedula);
		} catch (NumberFormatException e) {
			logger.error("Error Parametros Inválidos : " + e.getMessage());
			error = new Error(Errors.INVALID_PARAM.getCode(), Errors.INVALID_PARAM.getDescription());
		}
		return error;
	}

	private Response obtenerDatos() throws Exception {
		Response response = null;
		try {
			response = (Response) Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).ignoreHttpErrors(true)
					.execute();
			return response;
		} catch (Exception e) {
			logger.error("Error :" + e.getMessage());
			throw e;
		}
	}

	@Override
	public boolean verificarCedula(String cedula) throws AseguradoException {
		Document doc = null;
		try {
			doc = Jsoup.connect(url).data("nro_cic", cedula).data("recuperar", "Recuperar").data("envio", "ok")
					.userAgent("Mozilla/5.0").timeout(100000).post();
		} catch (Exception ex) {
			logger.error("Error : " + ex.getMessage());
			throw new AseguradoException(
					new Error(Errors.INTERNAL_ERROR.getCode(), Errors.INTERNAL_ERROR.getDescription()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		try {
			Integer.parseInt(cedula);

			try {
				if (doc != null) {
					Elements datosAsegurado = doc.select("td");
					if (datosAsegurado.size() < 7) {
						logger.info("Usuario con CI no existe ");
						throw new AseguradoException(
								new Error(Errors.NOT_EXIST.getCode(),
										Errors.USER.getDescription() + cedula + Errors.NOT_EXIST.getDescription()),
								HttpStatus.NOT_FOUND);
					}
					return true;
				}
			} catch (Exception e) {
				logger.error("Error Usuario con CI no existe : " + e.getMessage());
				throw new AseguradoException(
						new Error(Errors.NOT_EXIST.getCode(),
								Errors.USER.getDescription() + cedula + Errors.NOT_EXIST.getDescription()),
						HttpStatus.NOT_FOUND);
			}
		} catch (NumberFormatException n) {
			logger.error("Error Parametro Inválidos : " + n.getMessage());
			throw new AseguradoException(
					new Error(Errors.INVALID_PARAM.getCode(), Errors.INVALID_PARAM.getDescription()),
					HttpStatus.BAD_REQUEST);

		}
		return false;

	}

}
