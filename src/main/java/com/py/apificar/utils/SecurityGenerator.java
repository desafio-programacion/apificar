package com.py.apificar.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityGenerator {

	private static final Logger logger = LoggerFactory.getLogger(SecurityGenerator.class);

	public static String getKey(String key) {
		try {
			if (key == null) {
				return "";
			}	
			Mac mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
			mac.init(secret_key);

			String hash = Base64.encodeBase64String(mac.doFinal());

			return hash;
		} catch (InvalidKeyException ignored) {
			logger.error("Invalid key exception while converting to HMac SHA256");
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error :" + e.getMessage());
		}
		return ""; 
	}

}
