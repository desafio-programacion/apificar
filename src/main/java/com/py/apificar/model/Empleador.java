package com.py.apificar.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Empleador implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Nro. Patronal")
	@JacksonXmlProperty(localName = "Nro._Patronal")
	private String nroPatronal;
	
	@JsonProperty("Empleador")
	private String empleador;
	
	@JsonProperty("Estado")
	private String estado;
	
	@JsonProperty("Meses de aporte")
	@JacksonXmlProperty(localName = "Meses_de_aporte")
	private String mesesAporte;
	
	@JsonProperty("Vencimiento")
	private String vencimiento;
	
	@JsonProperty("Ultimo Periodo Abonado")
	@JacksonXmlProperty(localName = "Ultimo_Periodo_Abonado")
	private String ultimoPeriodoAbonado;

	public Empleador() {
	}

	public Empleador(String nroPatronal, String empleador, String estado, String mesesAporte, String vencimiento,
			String ultimoPeriodoAbonado) {
		super();
		this.nroPatronal = nroPatronal;
		this.empleador = empleador;
		this.estado = estado;
		this.mesesAporte = mesesAporte;
		this.vencimiento = vencimiento;
		this.ultimoPeriodoAbonado = ultimoPeriodoAbonado;
	}

	public String getNroPatronal() {
		return nroPatronal;
	}

	public void setNroPatronal(String nroPatronal) {
		this.nroPatronal = nroPatronal;
	}

	public String getEmpleador() {
		return empleador;
	}

	public void setEmpleador(String empleador) {
		this.empleador = empleador;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMesesAporte() {
		return mesesAporte;
	}

	public void setMesesAporte(String mesesAporte) {
		this.mesesAporte = mesesAporte;
	}

	public String getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

	public String getUltimoPeriodoAbonado() {
		return ultimoPeriodoAbonado;
	}

	public void setUltimoPeriodoAbonado(String ultimoPeriodoAbonado) {
		this.ultimoPeriodoAbonado = ultimoPeriodoAbonado;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empleador [Nro. Patronal=");
		builder.append(nroPatronal);
		builder.append(", Empleador=");
		builder.append(empleador);
		builder.append(", Estado=");
		builder.append(estado);
		builder.append(", Meses Aporte=");
		builder.append(mesesAporte);
		builder.append(", Vencimiento=");
		builder.append(vencimiento);
		builder.append(", Ultimo Periodo Abonado=");
		builder.append(ultimoPeriodoAbonado);
		builder.append("]");
		return builder.toString();
	}

	
	
	

}
