package com.py.apificar.controller;

import javax.servlet.http.HttpServletRequest;

import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.py.apificar.enums.Errors;
import com.py.apificar.error.AseguradoException;
import com.py.apificar.error.Error;
import com.py.apificar.model.Asegurado;
import com.py.apificar.service.AseguradoService;
import com.py.apificar.utils.HtmlConverter;
import com.py.apificar.utils.ValidateApiKey;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RestController
@RequestMapping("/consulta")
@Api("Consulta de Asegurado (IPS)")
public class AseguradoController {

	private static final String CI = "/{cedula}";

	@Autowired
	private AseguradoService aseguradoService;

	@Autowired
	private ValidateApiKey validateApiKey;

	@ApiOperation("Obtiene datos de un asegurado por nro. de cédula")
	@GetMapping(value = CI, produces = { MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_HTML_VALUE })
	public ResponseEntity<?> obtenerDatosAsegurado(final HttpServletRequest req, @PathVariable("cedula") String cedula)
			throws AseguradoException {

		if (validateApiKey.validate(req)) {
			boolean verifyCedula = aseguradoService.verificarCedula(cedula);
			if (verifyCedula) {

				Asegurado asegurado = aseguradoService.getAsegurado(cedula);
				String header = req.getHeader("Accept");

				switch (header) {
				case MediaType.TEXT_PLAIN_VALUE:
					return new ResponseEntity<String>(asegurado.toString(), HttpStatus.OK);
				case MediaType.TEXT_HTML_VALUE:
					Document doc = HtmlConverter.generateHtml(asegurado);
					return new ResponseEntity<String>(doc.toString(), HttpStatus.OK);
				default:
					return new ResponseEntity<Asegurado>(asegurado, HttpStatus.OK);
				}

			} else {
				return new ResponseEntity<Error>(
						new Error(Errors.INTERNAL_ERROR.getCode(), Errors.INTERNAL_ERROR.getDescription()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			throw new AseguradoException(
					new Error(Errors.NOT_AUTHORIZED.getCode(), Errors.NOT_AUTHORIZED.getDescription()),
					HttpStatus.FORBIDDEN);
		}
	}

	@ApiOperation("Inserción no implementada")
	@PostMapping()
	public ResponseEntity<?> insert() {
		return null;
	}

	@ApiOperation("Actualización no implementada")
	@PutMapping(value = CI)
	public ResponseEntity<?> update(@PathVariable("cedula") String cedula) {
		return null;
	}

	@ApiOperation("Eliminación no implementada")
	@DeleteMapping(value = CI)
	public ResponseEntity<?> delete(@PathVariable("cedula") String cedula) {
		return null;
	}
}
