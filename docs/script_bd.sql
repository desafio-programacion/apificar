/*Crear BD*/
create database asegurados;

/* Crear tabla*/
CREATE TABLE public.config
(
  id bigint NOT NULL,
  firma character varying(255),
  estado character varying(1) default 'A',
  CONSTRAINT id_pk PRIMARY KEY (id),
  CONSTRAINT firma UNIQUE (firma)
);


/* Insertar Datos */
INSERT INTO config  (id,firma,estado)
       VALUES (1, '1IXHwj4Dm1N0afpV0vKmB3SjCg4EMJo6MUbanu/A2IU=', 'A');
