package com.py.apificar.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "config")
public class Config implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Basic(optional = false)
	private String firma;

	@Basic(optional = false)
	private String estado;

	public Config() {
		super();
	}

	public Config(Long id, String firma, String estado) {
		super();
		this.id = id;
		this.firma = firma;
		this.estado = estado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Config [id=");
		builder.append(id);
		builder.append(", firma=");
		builder.append(firma);
		builder.append(", estado=");
		builder.append(estado);
		builder.append("]");
		return builder.toString();
	}

}
