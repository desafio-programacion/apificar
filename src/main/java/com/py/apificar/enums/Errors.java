package com.py.apificar.enums;

public enum Errors {

	USER("g000", "usuario con cédula "),
	NOT_EXIST("g100", " no existe "),
	INVALID_PARAM("g101", "Parámetros inválidos "), 
	INTERNAL_ERROR("g102", "Error interno del servidor "),
	NOT_AUTHORIZED("g103", "No autorizado ");

	private final String code;
	private final String description;
	
	private Errors(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	

}
