package com.py.apificar.error;

import org.springframework.http.HttpStatus;

public class AseguradoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Error error;
	private HttpStatus status;

	public AseguradoException(Error error, HttpStatus status) {
		super();
		this.error = error;
		this.status = status;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AseguradoException [error=");
		builder.append(error.toString());
		builder.append(", status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}

}
