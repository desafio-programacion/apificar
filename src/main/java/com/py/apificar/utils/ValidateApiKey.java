package com.py.apificar.utils;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.py.apificar.entity.Config;
import com.py.apificar.enums.Errors;
import com.py.apificar.error.AseguradoException;
import com.py.apificar.error.Error;
import com.py.apificar.repository.ConfigRepository;

@Component
public class ValidateApiKey {

	private static final Logger logger = LoggerFactory.getLogger(ValidateApiKey.class);

	@Autowired
	ConfigRepository configRepo;

	public boolean validate(HttpServletRequest req) throws AseguradoException {
		try {

			String apiKey = req.getHeader("x-api-key");
			if (apiKey != null && !apiKey.isEmpty()) {
				Optional<Config> config = configRepo.findByFirmaAndEstado(apiKey, "A");

				if (config.isPresent()) {
					logger.info("API KEY validado correctamente");
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new AseguradoException(
					new Error(Errors.NOT_AUTHORIZED.getCode(), Errors.NOT_AUTHORIZED.getDescription()),
					HttpStatus.FORBIDDEN);
		}
	}

}
